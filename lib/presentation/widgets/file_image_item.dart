import 'dart:io';

import 'package:flutter/material.dart';

class FileImageItem extends StatelessWidget {
  final String path;
  final VoidCallback onPressed;
  const FileImageItem({Key? key, required this.path, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Stack(
        children: [
          Image.file(
            File(path),
            width: 100,
            height: 100,
          ),
          Positioned(
            bottom: 0,
            left: 50,
            child: IconButton(
              onPressed: onPressed,
              icon: Icon(
                Icons.delete,
                size: 32,
                color: Theme.of(context).backgroundColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
