import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class NetworkImageItem extends StatelessWidget {
  final String path;
  final VoidCallback onRemovePressed;
  const NetworkImageItem(
      {Key? key, required this.path, required this.onRemovePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Stack(
        children: [
          Image.network(
            path,
            width: 100,
            height: 100,
          ),
          Positioned(
            bottom: 0,
            left: 50,
            child: IconButton(
              onPressed: onRemovePressed,
              icon: Icon(
                Icons.delete,
                size: 32,
                color: Theme.of(context).backgroundColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
