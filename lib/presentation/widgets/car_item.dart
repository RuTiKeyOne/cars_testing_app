import 'package:cars/core/model/car.dart';
import 'package:cars/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class CarItem extends StatelessWidget {
  final Car car;
  final VoidCallback onEditPressed;
  final VoidCallback onRemovePressed;
  const CarItem({
    Key? key,
    required this.car,
    required this.onEditPressed,
    required this.onRemovePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${S.of(context).manufacturer}: ${car.manufacturer}"),
                    const SizedBox(height: 5),
                    Text("${S.of(context).model}: ${car.model}"),
                    const SizedBox(height: 5),
                    Text(
                        "${S.of(context).yearOfRelease}: ${car.yearOfRelease}"),
                    const SizedBox(height: 5),
                    Text("${S.of(context).classType}: ${car.classType}"),
                    const SizedBox(height: 5),
                    Text("${S.of(context).bodyType}: ${car.bodyType}"),
                    const SizedBox(height: 5),
                  ],
                ),
                Row(
                  children: [
                    IconButton(
                      onPressed: onEditPressed,
                      icon: const Icon(
                        Icons.edit,
                        size: 32,
                      ),
                    ),
                    IconButton(
                      onPressed: onRemovePressed,
                      icon: const Icon(
                        Icons.delete,
                        size: 32,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            car.downloadURL.isNotEmpty
                ? const SizedBox(height: 5)
                : Container(),
            car.downloadURL.isNotEmpty
                ? Align(
                    alignment: Alignment.centerLeft,
                    child: Wrap(
                      children: car.downloadURL
                          .map((e) => Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                child: Image.network(
                                  e,
                                  width: 100,
                                  height: 100,
                                ),
                              ))
                          .toList(),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
