import 'package:cars/presentation/widgets/car_item.dart';
import '../../core/model/car.dart';
import '../../core/navigation/route_generator.dart';
import '../../generated/l10n.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AllCarsScreen extends StatefulWidget {
  const AllCarsScreen({Key? key}) : super(key: key);

  @override
  State<AllCarsScreen> createState() => _AllCarsScreenState();
}

class _AllCarsScreenState extends State<AllCarsScreen> {
  Stream<List<Car>> receiveCars() => FirebaseFirestore.instance
      .collection("cars")
      .snapshots()
      .map((event) => event.docs.map((e) => Car.fromMap(e.data())).toList());

  void removeCar(Car car) {
    final documentReference =
        FirebaseFirestore.instance.collection("cars").doc(car.id.toString());
    documentReference.delete();
  }

  Widget buildUser(Car car) => CarItem(
      car: car,
      onEditPressed: () {
        Navigator.of(context).pushNamed(addOrEditCarRouteName, arguments: {
          'car': car,
        });
      },
      onRemovePressed: () => removeCar(car));

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).cars),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () =>
              Navigator.of(context).pushNamed(addOrEditCarRouteName),
          child: const Icon(
            Icons.add,
          ),
        ),
        body: StreamBuilder<List<Car>>(
          stream: receiveCars(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            if (snapshot.hasData) {
              final users = snapshot.data!;
              return ListView(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                children: users.map((e) => buildUser(e)).toList(),
              );
            }
            return const Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
