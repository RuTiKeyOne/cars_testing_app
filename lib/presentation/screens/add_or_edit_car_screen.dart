import 'dart:io';
import 'dart:math';
import 'package:cars/presentation/widgets/file_image_item.dart';
import 'package:cars/presentation/widgets/network_image_item.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import '../../core/model/car.dart';
import '../../generated/l10n.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddOrEditCarScreen extends StatefulWidget {
  final Car? car;
  const AddOrEditCarScreen({Key? key, this.car}) : super(key: key);

  @override
  State<AddOrEditCarScreen> createState() => _AddOrEditCarScreenState();
}

class _AddOrEditCarScreenState extends State<AddOrEditCarScreen> {
  late final ImagePicker _picker;
  late List<String> fileImagesPath;
  late List<String> networkImagesPath;
  late final Random random;
  late final GlobalKey<FormState> formKey;
  late final TextEditingController yearOfReleaseTextController;
  late final TextEditingController modelTextController;
  late final TextEditingController manufactureTextController;
  late final TextEditingController classTypeTextController;
  late final TextEditingController bodyTypeTextController;

  @override
  void initState() {
    fileImagesPath = [];
    networkImagesPath = [];
    random = Random();
    formKey = GlobalKey<FormState>();
    _picker = ImagePicker();

    if (widget.car != null) {
      _initializeContollersWithNotNullCar();
    } else {
      _initializeContollersWithNullCar();
    }
    super.initState();
  }

  void _initializeContollersWithNullCar() {
    yearOfReleaseTextController = TextEditingController();
    modelTextController = TextEditingController();
    manufactureTextController = TextEditingController();
    classTypeTextController = TextEditingController();
    bodyTypeTextController = TextEditingController();
  }

  void _initializeContollersWithNotNullCar() {
    networkImagesPath = widget.car!.downloadURL;
    yearOfReleaseTextController =
        TextEditingController(text: widget.car!.yearOfRelease.toString());
    yearOfReleaseTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: yearOfReleaseTextController.text.length));

    modelTextController = TextEditingController(text: widget.car!.model);
    modelTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: modelTextController.text.length));

    manufactureTextController =
        TextEditingController(text: widget.car!.manufacturer);
    manufactureTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: manufactureTextController.text.length));

    classTypeTextController =
        TextEditingController(text: widget.car!.classType);
    classTypeTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: classTypeTextController.text.length));

    bodyTypeTextController = TextEditingController(text: widget.car!.bodyType);
    bodyTypeTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: bodyTypeTextController.text.length));
  }

  String? yearOfReleaseTextValidator(String? val, BuildContext context) {
    if (val == null || val.isEmpty) {
      return S.of(context).empty;
    }

    final value = int.tryParse(val);
    if (value == null) {
      return S.of(context).year_of_release_validate_message_1;
    }

    return null;
  }

  String? validateIsEmptyField(String? val, BuildContext context) {
    if (val == null || val.isEmpty) {
      return S.of(context).empty;
    }
    return null;
  }

  String receiveRandomString(int length) {
    const characters =
        '+-*=?AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz';
    Random random = Random();
    return String.fromCharCodes(Iterable.generate(length,
        (_) => characters.codeUnitAt(random.nextInt(characters.length))));
  }

  void createAndAddCar() async {
    if (formKey.currentState!.validate()) {
      Navigator.of(context).pop();
      late final List<String>? downloadUrls;
      if (fileImagesPath.isNotEmpty) {
        downloadUrls = await fetchDownloadURL(fileImagesPath);
      } else {
        downloadUrls = [];
      }
      final car = Car(
          id: receiveRandomString(50),
          downloadURL: downloadUrls,
          yearOfRelease: int.parse(yearOfReleaseTextController.text),
          model: modelTextController.text,
          manufacturer: manufactureTextController.text,
          classType: classTypeTextController.text,
          bodyType: bodyTypeTextController.text);
      final docCar =
          FirebaseFirestore.instance.collection('cars').doc(car.id.toString());
      final json = car.toMap();
      yearOfReleaseTextController.clear();
      modelTextController.clear();
      manufactureTextController.clear();
      classTypeTextController.clear();
      bodyTypeTextController.clear();
      fetchDownloadURL(fileImagesPath);
      await docCar.set(json);
    }
  }

  void updateCar(Car car) async {
    if (formKey.currentState!.validate()) {
      Navigator.of(context).pop();
      late final List<String>? downloadUrls;
      if (fileImagesPath.isNotEmpty) {
        downloadUrls = await fetchDownloadURL(fileImagesPath);
      } else {
        downloadUrls = [];
      }
      final Car updatedCar = car.copyWith(
          yearOfRelease: int.parse(yearOfReleaseTextController.text),
          model: modelTextController.text,
          manufacturer: manufactureTextController.text,
          classType: classTypeTextController.text,
          bodyType: bodyTypeTextController.text,
          downloadURL: [...networkImagesPath, ...downloadUrls]);
      final map = updatedCar.toMap();
      final docCar =
          FirebaseFirestore.instance.collection('cars').doc(car.id.toString());
      docCar.update(map);
    }
  }

  void pickImage(ImageSource source) async {
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      fileImagesPath.add(image.path);
      setState(() {});
    }
  }

  void removeFileImage(String path) {
    fileImagesPath.remove(path);
    setState(() {});
  }

  void removeNetworkImage(String path) {
    networkImagesPath.remove(path);
    setState(() {});
  }

  Future<List<String>> fetchDownloadURL(List<String> imagesPath) async {
    List<String> urls = [];
    FirebaseStorage storage = FirebaseStorage.instance;
    for (String path in imagesPath) {
      File imageFile = File(path);
      final reference = storage.ref().child(imageFile.path);
      UploadTask task = reference.putFile(imageFile);
      String url = await (await task.whenComplete(() {})).ref.getDownloadURL();
      urls.add(url);
    }
    return urls;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.car == null
              ? S.of(context).add_car
              : S.of(context).update_data),
        ),
        body: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Form(
                key: formKey,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Column(
                    children: [
                      fileImagesPath.isNotEmpty
                          ? const SizedBox(height: 10)
                          : Container(),
                      fileImagesPath.isNotEmpty &&
                              (widget.car == null ||
                                  widget.car!.downloadURL.isEmpty)
                          ? Align(
                              alignment: Alignment.centerLeft,
                              child: Wrap(
                                children: fileImagesPath
                                    .map((e) => FileImageItem(
                                        path: e,
                                        onPressed: () => removeFileImage(e)))
                                    .toList(),
                              ),
                            )
                          : Container(),
                      networkImagesPath.isNotEmpty
                          ? Align(
                              alignment: Alignment.centerLeft,
                              child: Wrap(
                                children: networkImagesPath
                                    .map((e) => NetworkImageItem(
                                        path: e,
                                        onRemovePressed: () =>
                                            removeNetworkImage(e)))
                                    .toList(),
                              ),
                            )
                          : Container(),
                      const SizedBox(height: 10),
                      TextFormField(
                        validator: (val) =>
                            yearOfReleaseTextValidator(val, context),
                        onChanged: (val) => formKey.currentState!.validate(),
                        controller: yearOfReleaseTextController,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          labelText: S.of(context).enter_your_year_of_release,
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        validator: (val) => validateIsEmptyField(val, context),
                        controller: modelTextController,
                        onChanged: (val) => formKey.currentState!.validate(),
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          labelText: S.of(context).enter_your_model,
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        validator: (val) => validateIsEmptyField(val, context),
                        controller: manufactureTextController,
                        onChanged: (val) => formKey.currentState!.validate(),
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          labelText: S.of(context).enter_your_manufacturer,
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        onChanged: (val) => formKey.currentState!.validate(),
                        validator: (val) => validateIsEmptyField(val, context),
                        controller: classTypeTextController,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          labelText: S.of(context).enter_your_class_type,
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        onChanged: (val) => formKey.currentState!.validate(),
                        validator: (val) => validateIsEmptyField(val, context),
                        controller: bodyTypeTextController,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          labelText: S.of(context).enter_your_body_type,
                        ),
                      ),
                      const Spacer(),
                      widget.car != null
                          ? Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 5),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: ElevatedButton(
                                    onPressed: () => updateCar(widget.car!),
                                    child: Text(S.of(context).update)),
                              ),
                            )
                          : Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 5),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: ElevatedButton(
                                    onPressed: () async => createAndAddCar(),
                                    child: Text(S.of(context).add)),
                              ),
                            ),
                      networkImagesPath.isEmpty
                          ? Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 5),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: ElevatedButton(
                                    onPressed: () async =>
                                        pickImage(ImageSource.gallery),
                                    child: Text(S.of(context).add_image)),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
