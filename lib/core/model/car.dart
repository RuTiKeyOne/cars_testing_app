import 'dart:convert';

import 'package:flutter/foundation.dart';

class Car {
  final String id;
  final int yearOfRelease;
  final String model;
  final String manufacturer;
  final String classType;
  final String bodyType;
  final List<String> downloadURL;
  Car({
    required this.id,
    required this.yearOfRelease,
    required this.model,
    required this.manufacturer,
    required this.classType,
    required this.bodyType,
    required this.downloadURL,
  });

  Car copyWith({
    String? id,
    int? yearOfRelease,
    String? model,
    String? manufacturer,
    String? classType,
    String? bodyType,
    List<String>? downloadURL,
  }) {
    return Car(
      id: id ?? this.id,
      yearOfRelease: yearOfRelease ?? this.yearOfRelease,
      model: model ?? this.model,
      manufacturer: manufacturer ?? this.manufacturer,
      classType: classType ?? this.classType,
      bodyType: bodyType ?? this.bodyType,
      downloadURL: downloadURL ?? this.downloadURL,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'yearOfRelease': yearOfRelease,
      'model': model,
      'manufacturer': manufacturer,
      'classType': classType,
      'bodyType': bodyType,
      'downloadURL': downloadURL,
    };
  }

  factory Car.fromMap(Map<String, dynamic> map) {
    return Car(
      id: map['id'] ?? '',
      yearOfRelease: map['yearOfRelease']?.toInt() ?? 0,
      model: map['model'] ?? '',
      manufacturer: map['manufacturer'] ?? '',
      classType: map['classType'] ?? '',
      bodyType: map['bodyType'] ?? '',
      downloadURL: List<String>.from(map['downloadURL']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Car.fromJson(String source) => Car.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Car(id: $id, yearOfRelease: $yearOfRelease, model: $model, manufacturer: $manufacturer, classType: $classType, bodyType: $bodyType, downloadURL: $downloadURL)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Car &&
        other.id == id &&
        other.yearOfRelease == yearOfRelease &&
        other.model == model &&
        other.manufacturer == manufacturer &&
        other.classType == classType &&
        other.bodyType == bodyType &&
        listEquals(other.downloadURL, downloadURL);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        yearOfRelease.hashCode ^
        model.hashCode ^
        manufacturer.hashCode ^
        classType.hashCode ^
        bodyType.hashCode ^
        downloadURL.hashCode;
  }
}
