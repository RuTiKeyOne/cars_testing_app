import 'package:flutter/material.dart';
import '../../presentation/screens/add_or_edit_car_screen.dart';
import '../../presentation/screens/all_cars_screen.dart';
import '../model/car.dart';

const allCarsRouteName = '/all_cars';
const addOrEditCarRouteName = '/add_car';

class RouteGenerator {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case allCarsRouteName:
        return MaterialPageRoute(
          builder: ((context) => const AllCarsScreen()),
        );

      case addOrEditCarRouteName:
        late final Car? car;
        if (args != null && (args as Map<String, dynamic>)['car'] != null) {
          car = args['car'] as Car;
        } else {
          car = null;
        }
        return MaterialPageRoute(
          builder: ((context) => AddOrEditCarScreen(
                car: car,
              )),
        );

      default:
        return null;
    }
  }
}
