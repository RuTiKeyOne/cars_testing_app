// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "add": MessageLookupByLibrary.simpleMessage("Добавить"),
        "add_car": MessageLookupByLibrary.simpleMessage("Добавить машину"),
        "add_image":
            MessageLookupByLibrary.simpleMessage("Добавить изображение"),
        "bodyType": MessageLookupByLibrary.simpleMessage("Тип кузова"),
        "cars": MessageLookupByLibrary.simpleMessage("Машины"),
        "classType": MessageLookupByLibrary.simpleMessage("Класс"),
        "empty": MessageLookupByLibrary.simpleMessage("Пусто"),
        "enter_your_body_type":
            MessageLookupByLibrary.simpleMessage("Введите тип кузова"),
        "enter_your_class_type":
            MessageLookupByLibrary.simpleMessage("Введите класс авто"),
        "enter_your_manufacturer":
            MessageLookupByLibrary.simpleMessage("Введите производителя"),
        "enter_your_model":
            MessageLookupByLibrary.simpleMessage("Введите вашу модель"),
        "enter_your_year_of_release":
            MessageLookupByLibrary.simpleMessage("Введите год выпуска"),
        "manufacturer": MessageLookupByLibrary.simpleMessage("Производитель"),
        "model": MessageLookupByLibrary.simpleMessage("Модель"),
        "no_data": MessageLookupByLibrary.simpleMessage("Нет данных"),
        "update": MessageLookupByLibrary.simpleMessage("Обновить"),
        "update_data": MessageLookupByLibrary.simpleMessage("Обновить данные"),
        "yearOfRelease":
            MessageLookupByLibrary.simpleMessage("Год производства"),
        "year_of_release_validate_message_1":
            MessageLookupByLibrary.simpleMessage(
                "Значение года выпуска должно состоять из чисел")
      };
}
