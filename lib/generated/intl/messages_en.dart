// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "add": MessageLookupByLibrary.simpleMessage("Add"),
        "add_car": MessageLookupByLibrary.simpleMessage("Add car"),
        "add_image": MessageLookupByLibrary.simpleMessage("Add image"),
        "bodyType": MessageLookupByLibrary.simpleMessage("Body type"),
        "cars": MessageLookupByLibrary.simpleMessage("Cars"),
        "classType": MessageLookupByLibrary.simpleMessage("Class type"),
        "empty": MessageLookupByLibrary.simpleMessage("Empty"),
        "enter_your_body_type":
            MessageLookupByLibrary.simpleMessage("Enter your body type"),
        "enter_your_class_type":
            MessageLookupByLibrary.simpleMessage("Enter your class type"),
        "enter_your_manufacturer":
            MessageLookupByLibrary.simpleMessage("Enter your manufacturer"),
        "enter_your_model":
            MessageLookupByLibrary.simpleMessage("Enter your model"),
        "enter_your_year_of_release":
            MessageLookupByLibrary.simpleMessage("Enter your year of release"),
        "manufacturer": MessageLookupByLibrary.simpleMessage("Manufacturer"),
        "model": MessageLookupByLibrary.simpleMessage("Model"),
        "no_data": MessageLookupByLibrary.simpleMessage("No data"),
        "update": MessageLookupByLibrary.simpleMessage("Update"),
        "update_data": MessageLookupByLibrary.simpleMessage("Update data"),
        "yearOfRelease":
            MessageLookupByLibrary.simpleMessage("Year Of Release"),
        "year_of_release_validate_message_1":
            MessageLookupByLibrary.simpleMessage(
                "The value of the year of release should consist of numbers")
      };
}
